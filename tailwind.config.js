/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "",
        secondary: "",
        background: "#1A202C",
        border: "#2D3748",
        backgroundDark: "#171923",
        whitecc: "#FFFFFFCC",
        carouselNotActive: "#d9d9d999",
        carouselActive: "#D9D9D9",
      },
    },
  },
  plugins: [],
};
