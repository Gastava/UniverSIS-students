import Head from "next/head";
import Navbar from "../common/Navbar";

type DefaultLayoutProps = {
  children: React.ReactNode;
};

export default function DefaultLayout({ children }: DefaultLayoutProps) {
  return (
    <div className="min-h-screen bg-background">
      <Head>
        <title>Universis Project | Student Portal</title>
        <meta
          name="description"
          content="An alternative student portal for Universis Project, built with React and Next.js."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="fixed left-0 z-50 w-full">
        <Navbar />
      </div>
      <main className="p-2 pt-24 md:pl-16 max-w-[1000px] mx-auto mb-16 sm:mb-0">{children}</main>
    </div>
  );
}
