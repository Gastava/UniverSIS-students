import GradeCard from "@/components/common/GradeCard";
import PeriodProgress from "@/components/common/PeriodProgress";
import SectionCard from "@/components/common/SectionCard";
import TotalTracker from "@/components/common/TotalTracker";
import { calculateStatsFromCourseGroup } from "@/utilities/calculateStatsFromCourseGroup";
import { Heading, Text } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

interface AllGradesProps {
  groups: any;
  groupingType: "semester" | "courseType";
  courses: any;
}

export default function AllGrades({ groups, groupingType, courses }: AllGradesProps) {
  const { t } = useTranslation("grades");
  const { i18n } = useTranslation();

  const groupsWithCourses = groups.map((group: any) => {
    return {
      ...group,
      coursesOfGroup: courses.filter((course: any) => course[groupingType].id === group.id),
    };
  });

  const currentLanguage = i18n.language;

  return (
    <div className="flex flex-col gap-8">
      {groupsWithCourses.map((group: any) => {
        const { passed, total, passedECTS, totalECTS, average } = calculateStatsFromCourseGroup(group.coursesOfGroup);

        const periodSatistics = [
          {
            label: `${t("Grades.PassedCourses")}`,
            value: `${passed}/${total}`,
          },
          {
            label: `${t("Grades.GradeSimpleAverage")}`,
            value: `${average.toFixed(2)}`,
          },
          { label: `${t("Grades.ECTS")}`, value: `${passedECTS}/${totalECTS}` },
        ];
        console.log(group);

        return (
          group.coursesOfGroup.length > 0 && (
            <SectionCard
              key={group.id}
              header={
                <Heading textAlign="center">
                  {currentLanguage === "en"
                    ? `${group.id} Semester`
                    : currentLanguage === "el"
                    ? `${group.name}`
                    : `${group.id} Semester`}
                </Heading>
              }
            >
              <div className="flex flex-col gap-4 mb-2">
                <PeriodProgress passed={passed} total={total} />
                <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
                  {group.coursesOfGroup.map((course: any) => {
                    const drawerContent = (
                      <div>
                        <div className="flex justify-between">
                          <Text>
                            {t("CourseType")} : <span className="text-gray-500">{course.courseType.abbreviation}</span>
                          </Text>
                          <Text>
                            {t("ECTS")} : <span className="text-gray-500">{course.course.ects}</span>
                          </Text>
                          <Text>
                            {t("Grades.ExamPeriod")} :{" "}
                            <span className="text-gray-500">
                              {course.gradePeriodDescription + course.gradeYear?.id}
                            </span>
                          </Text>
                        </div>
                        <Text>
                          {t("Instructor")} :{" "}
                          <span className="text-gray-500">
                            {course.course.instructor?.givenName + " " + course.course.instructor?.familyName}
                          </span>
                        </Text>
                      </div>
                    );

                    return (
                      <GradeCard
                        key={course.id}
                        title={course.courseTitle}
                        grade={course.formattedGrade}
                        isPassed={course.isPassed}
                        code={course.course.displayCode}
                        courseExamId={course.gradeExam}
                        drawer={drawerContent}
                      />
                    );
                  })}
                </div>
                <TotalTracker stats={periodSatistics} />
              </div>
            </SectionCard>
          )
        );
      })}
    </div>
  );
}
