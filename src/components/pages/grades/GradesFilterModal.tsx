import Button from "@/components/common/Button";
import { Filters } from "@/pages/grades";
import {
  Divider,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Radio,
  RadioGroup,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Image from "next/image";
import { useState } from "react";
import { useTranslation } from "react-i18next";

function GradesFiltersModal({
  filters,
  setFilters,
}: {
  filters: Filters;
  setFilters: React.Dispatch<React.SetStateAction<Filters>>;
}) {
  const { t } = useTranslation("grades");
  const [tempFilters, setTempFilters] = useState<Filters>(filters);

  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleStatusChange = (value: Filters["statusOfCourses"]) => {
    setTempFilters({ ...tempFilters, statusOfCourses: value });
  };

  const handleGroupingChange = (value: Filters["typeOfGrouping"]) => {
    setTempFilters({ ...tempFilters, typeOfGrouping: value });
  };

  const handleSave = () => {
    setFilters(tempFilters);
    onClose();
  };

  const handleModalClose = () => {
    setTempFilters(filters);
    onClose();
  };

  return (
    <>
      <div
        onClick={onOpen}
        className="border border-whitecc bg-background rounded-lg px-6 py-2 flex"
      >
        <Image
          src="/icons/filter-open.svg"
          width={20}
          height={20}
          alt="filter"
        />
      </div>

      <Modal preserveScrollBarGap isOpen={isOpen} onClose={handleModalClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{t("Grades.Filter")}</ModalHeader>
          <ModalCloseButton />

          <ModalBody px={0} py={2}>
            <Stack gap={8}>
              <Divider />
              <RadioGroup
                onChange={handleStatusChange}
                value={tempFilters.statusOfCourses}
                px={6}
              >
                <div className="grid grid-cols-2 gap-4 text-nowrap">
                  <Radio value="all">{t("Grades.AllCourses")}</Radio>
                  {/* <Radio value="pending">{t("Grades.PendingCourses")}</Radio> */}
                  <Radio value="passed">{t("Grades.OnlyPassed")}</Radio>
                  <Radio value="incomplete">{t("Grades.FailedCourses")}</Radio>
                  <Radio disabled value="thesis">
                    {t("Grades.OnlyThesis")}
                  </Radio>
                </div>
              </RadioGroup>
              <Divider />
              <Text px={6}>{t("Grades.OrderBy")}</Text>
              <Divider />
              <RadioGroup
                onChange={handleGroupingChange}
                value={tempFilters.typeOfGrouping}
                px={6}
              >
                <div className="grid grid-cols-2 gap-4 text-nowrap">
                  <Radio value="semester">{t("Grades.Semester")}</Radio>
                  <Radio value="courseType">{t("CourseType")}</Radio>
                </div>
              </RadioGroup>
              <Divider />
            </Stack>
          </ModalBody>

          <ModalFooter>
            <Button
              icon="/icons/checkmark.svg"
              handleClick={handleSave}
              text={t("Save")}
            />
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default GradesFiltersModal;
