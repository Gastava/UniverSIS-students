import SectionCard from "@/components/common/SectionCard";
import { useBreakpoint } from "@chakra-ui/react";
import { useState } from "react";
import Progress from "../dashboard/headerCards/Progress";
import Roadmap from "../dashboard/headerCards/Roadmap";

// ! copy from dashboard, should be refactored to a shared component
const Controls = ({
  activeItem,
  setActiveItem,
}: {
  activeItem: number;
  setActiveItem: React.Dispatch<React.SetStateAction<number>>;
}) => {
  return (
    <div className="flex justify-center gap-2 mb-2">
      <button
        className={`p-2 rounded-full ${activeItem === 0 ? "bg-carouselActive px-4" : "bg-carouselNotActive"}`}
        onClick={() => setActiveItem(0)}
      />
      <button
        className={`p-2 rounded-full ${activeItem === 1 ? "bg-carouselActive px-4" : "bg-carouselNotActive"}`}
        onClick={() => setActiveItem(1)}
      />
    </div>
  );
};

function HeaderCards() {
  const [activeItem, setActiveItem] = useState(0);
  const breakpoint = useBreakpoint();

  // todo: should switch to actual carousel component which chakra does not have
  if (breakpoint === "base" || breakpoint === "sm") {
    return (
      <div className="flex flex-col gap-2">
        <SectionCard noPadding>
          <div className="h-[110px] items-center">
            {activeItem === 0 && <Progress />}
            {activeItem === 1 && <Roadmap />}
          </div>
          <Controls activeItem={activeItem} setActiveItem={setActiveItem} />
        </SectionCard>
      </div>
    );
  }

  return (
    <div className="grid grid-cols-2 gap-4 md:gap-6">
      <SectionCard noPadding>
        <Roadmap />
      </SectionCard>
      <SectionCard noPadding>
        <Progress />
      </SectionCard>
    </div>
  );
}

export default HeaderCards;
