import { useTranslation } from "react-i18next";
import {
  useGetAllReadMessages,
  useGetAllUnreadMessages,
} from "@/queries/useGetMessages";
import MessageList from "./MessageList";

const AllMessages = () => {
  const allReadMessages = useGetAllReadMessages();
  const allUnreadMessages = useGetAllUnreadMessages();
  const { t } = useTranslation("messages");

  return (
    <div className="flex flex-col gap-5">
      <MessageList
        messages={allUnreadMessages}
        title={`${t("Messages.NewNotification")} ${t("Messages.messages")}`}
        envelope="envelope.svg"
      />

      <MessageList
        messages={allReadMessages}
        title={`${t("Messages.ReadMessages")} ${t("Messages.messages")}`}
        envelope="envelopeOpen.svg"
      />
    </div>
  );
};

export default AllMessages;
