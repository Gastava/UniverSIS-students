import React, { useState } from "react";
import { Text, Image, Button, Box } from "@chakra-ui/react";
import MessageCard from "@/components/common/MessageCard";
import MessagesContainer from "@/components/common/MessagesContainer";
import { useTranslation } from "react-i18next";

interface Message {
  action: { name: string };
  dateCreated: string;
  dateReceived: string;
  body: string;
  subject: string;
}

interface Props {
  messages: Message[];
  title: string;
  envelope: string;
}

const formatDate = (dateString: string): string => {
  const date = new Date(dateString);
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "short",
    day: "numeric",
  };
  return date.toLocaleDateString("en-US", options);
};

const MessageList: React.FC<Props> = ({ messages, title, envelope }) => {
  const { t } = useTranslation("messages");

  //! EDW TO CONTENT DRAWER ??
  const drawerContent = <Text>edw content drawer</Text>;

  return (
    <MessagesContainer title={title} numberOfMessages={messages.length}>
      {messages.length === 0 ? (
        <Text textAlign="center">{t("Messages.NoMessages")}</Text>
      ) : (
        messages.map((message, index) => (
          <MessageCard
            read={!!message.dateReceived}
            key={index}
            drawerContent={drawerContent}
          >
            <div className="flex flex-col w-full gap-4 ">
              <div className="flex justify-between items-center">
                <div className="flex flex-wrap items-center gap-2">
                  <Text fontSize="md" fontWeight="bold">
                    {message.action.name}
                  </Text>
                  <Text fontSize="xs">{`(${formatDate(
                    message.dateReceived
                  )})`}</Text>
                </div>
                <div className="flex flex-wrap  justify-end">
                  <div className="bg-gray-700 rounded-lg p-1">
                    <Image
                      src={`/icons/${envelope}`}
                      alt="envelope-icon"
                      width={10}
                      height={10}
                    />
                  </div>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div className="flex ">
                  <Text fontSize="xs">{`${t(
                    "Messages.ReplyFromSecretariat"
                  )} (${formatDate(message.dateReceived)})`}</Text>
                </div>
                <div className="flex gap-2 justify-between">
                  <div className="flex gap-2">
                    <div className="border-l border-gray-500" />
                    <Text fontSize="xs">{message.body}</Text>
                  </div>
                  <div className="flex gap-2 items-center">
                    <div className="border-l border-gray-500 h-full" />
                    <Button
                      size="sm"
                      color="white"
                      variant="outline"
                      _hover={{}}
                      _active={{}}
                    >
                      Download
                    </Button>
                  </div>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div>
                  <Text fontSize="xs">{`${t(
                    "Messages.ReplyFromUser"
                  )} (${formatDate(message.dateCreated)})`}</Text>
                </div>
                <div className="flex gap-2 ">
                  <div className="border-l border-gray-500" />
                  <Text fontSize="xs">User Message</Text>
                </div>
              </div>
            </div>
          </MessageCard>
        ))
      )}
    </MessagesContainer>
  );
};

export default MessageList;
