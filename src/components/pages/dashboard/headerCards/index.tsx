import SectionCard from "@/components/common/SectionCard";
import { useBreakpoint } from "@chakra-ui/react";
import { useState } from "react";
import Notifications from "./Notifications";
import Progress from "./Progress";
import Roadmap from "./Roadmap";

const Controls = ({
  activeItem,
  setActiveItem,
}: {
  activeItem: number;
  setActiveItem: React.Dispatch<React.SetStateAction<number>>;
}) => {
  return (
    <div className="flex justify-center gap-2 mb-2">
      <button
        className={`p-2 rounded-full ${activeItem === 0 ? "bg-carouselActive px-4" : "bg-carouselNotActive"}`}
        onClick={() => setActiveItem(0)}
      />
      <button
        className={`p-2 rounded-full ${activeItem === 1 ? "bg-carouselActive px-4" : "bg-carouselNotActive"}`}
        onClick={() => setActiveItem(1)}
      />
      <button
        className={`p-2 rounded-full ${activeItem === 2 ? "bg-carouselActive px-4" : "bg-carouselNotActive"}`}
        onClick={() => setActiveItem(2)}
      />
    </div>
  );
};

function HeaderCards() {
  const [activeItem, setActiveItem] = useState(0);
  // useBreakpoint returns "base" to work with servers side rendering and this causes a flicker of the mobile view
  // since we are exporting static files to serve the students app we can disable it
  const breakpoint = useBreakpoint({ ssr: false });

  // todo: should switch to actual carousel component which chakra does not have
  if (breakpoint === "base" || breakpoint === "sm") {
    return (
      <div className="flex flex-col gap-2">
        <SectionCard noPadding>
          <div className="h-[110px] items-center">
            {activeItem === 0 && <Notifications />}
            {activeItem === 1 && <Progress />}
            {activeItem === 2 && <Roadmap />}
          </div>
          <Controls activeItem={activeItem} setActiveItem={setActiveItem} />
        </SectionCard>
      </div>
    );
  }

  return (
    <div className="grid grid-cols-2 gap-4 md:gap-6">
      <div className="col-span-2">
        <SectionCard noPadding>
          <Roadmap />
        </SectionCard>
      </div>
      <SectionCard noPadding>
        <Notifications />
      </SectionCard>
      <SectionCard noPadding>
        <Progress />
      </SectionCard>
    </div>
  );
}

export default HeaderCards;
