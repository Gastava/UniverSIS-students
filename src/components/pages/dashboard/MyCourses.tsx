import CourseCard from "@/components/common/CourseCard";
import PanelCard from "@/components/common/PanelCard";
import useGetRecentCourses from "@/queries/useGetRecentCourses";
import { useTranslation } from "react-i18next";

const capitalizeFirstLetter = (string: string) => {
  if (!string) return "";
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const MyCourses = () => {
  const { currentRegistration } = useGetRecentCourses();
  const { t, i18n } = useTranslation("dashboard");
  const currentLanguage = i18n.language;

  return (
    <PanelCard
      title={t("iDashboard.MyCourses")}
      period={
        currentLanguage === "el"
          ? `${currentRegistration?.registrationPeriod?.name} ${currentRegistration?.registrationYear?.name}`
          : `${capitalizeFirstLetter(
              currentRegistration?.registrationPeriod?.alternateName
            )} ${currentRegistration?.registrationYear?.alternateName}`
      }
    >
      <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
        {currentRegistration.classes?.map((course: any) => (
          <CourseCard
            key={course.id}
            code={course.courseClass.course.displayCode}
            teachers={
              course.courseClass.instructors?.map(
                (instructor: any) =>
                  instructor.instructor.givenName +
                  " " +
                  instructor.instructor.familyName
              ) ?? []
            }
            title={course.courseClass.title}
          />
        ))}
      </div>
    </PanelCard>
  );
};

export default MyCourses;
