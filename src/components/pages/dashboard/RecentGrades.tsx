import PanelCard from "@/components/common/PanelCard";
import PeriodProgress from "@/components/common/PeriodProgress";
import useGetRecentGrades from "@/queries/useGetRecentGrades";
import { Flex, Text } from "@chakra-ui/react";
import GradeCard from "../../common/GradeCard";
import { useTranslation } from "react-i18next";

const RecentGrades = () => {
  const { t, i18n } = useTranslation("dashboard");
  const { recentGrades, examYear, examPeriod, examPeriodAlternate } =
    useGetRecentGrades();

  const capitalizeFirstLetter = (string: string) => {
    if (!string) return "";
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  const currentLanguage = i18n.language;
  const passedCourses = recentGrades.filter((course) => course.isPassed).length;

  return (
    <PanelCard
      title={t("iDashboard.MyRecentGrades")}
      period={
        currentLanguage === "el"
          ? examPeriod + " " + examYear
          : capitalizeFirstLetter(examPeriodAlternate).slice(0, -5) +
            " " +
            examYear
      }
    >
      <Flex align={"center"} gap={2} mb={2.5}>
        <PeriodProgress passed={passedCourses} total={recentGrades.length} />
        <Text>
          {passedCourses}/{recentGrades.length}
        </Text>
      </Flex>
      <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
        {recentGrades.map((gradeRecord) => (
          <GradeCard
            key={gradeRecord.id}
            title={gradeRecord.course.name}
            teachers={
              gradeRecord.courseClass.instructors?.map(
                (instructor: any) =>
                  instructor.instructor.givenName +
                  " " +
                  instructor.instructor.familyName
              ) ?? []
            }
            grade={gradeRecord.formattedGrade}
            isPassed={gradeRecord.isPassed}
            code={gradeRecord.course.displayCode}
            courseExamId={gradeRecord.courseExam.toString()}
          />
        ))}
      </div>
    </PanelCard>
  );
};

export default RecentGrades;
