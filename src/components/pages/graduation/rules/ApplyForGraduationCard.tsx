import Button from "@/components/common/Button";
import SectionCard from "@/components/common/SectionCard";
import { Heading, Text } from "@chakra-ui/react";

const ApplyGraduation = ({ successful }: { successful: boolean }) => {
  return (
    <SectionCard header={<Heading textAlign="center">Apply for Graduation</Heading>}>
      <div className="card flex flex-col items-center p-4">
        {successful ? (
          <Text textAlign="center" mb="4">
            The Graduation event is
            <span className="text-green-600"> currently available</span> in your department.
          </Text>
        ) : (
          <Text textAlign="center" mb="4">
            The Graduation event is not currently available in your department.
          </Text>
        )}

        <Button
          text="Application Wizard"
          icon={successful ? "/icons/down-exit-green.svg" : "/icons/down-exit.svg"}
          isRightIcon
          handleClick={() => {
            console.log("Application Wizard");
          }}
          chakraProps={successful ? { color: "green", colorScheme: "green" } : { disabled: true }}
        />
      </div>
    </SectionCard>
  );
};

export default ApplyGraduation;
