import SectionCard from "@/components/common/SectionCard";
import { Heading, Text } from "@chakra-ui/react";

function ComplexPrerequisites({ graduationRules }: { graduationRules: any[] }) {
  return (
    <SectionCard header={<Heading textAlign="center">Complex Prerequisites</Heading>}>
      <div className="card divide-y">
        <div className="grid grid-cols-6 py-4">
          <Heading className="col-span-3 md:col-span-1" textAlign="center" size="md">
            CONDITIONS
          </Heading>
          <Heading className="col-span-3 md:col-span-5" textAlign="center" size="md">
            RULES
          </Heading>
        </div>
        {graduationRules?.map((rule) => {
          // const { refersTo } = rule; // actually map tree structure
          return <Condition key={rule.id} rule={rule} />;
        })}
      </div>
    </SectionCard>
  );
}

const Condition = ({ rule }: { rule: any }) => {
  const { result, value1 } = rule.validationResult.data;
  const { message } = rule.validationResult;
  return (
    <div className="grid grid-cols-6 py-4">
      <Text
        className="col-span-2 md:col-span-1 flex justify-center items-center font-semibold"
        textColor={rule.validationResult.success ? "green.600" : "red.500"}
        textAlign="center"
      >
        {result}
        {value1 && <Text as="span">/{value1}</Text>}
      </Text>
      <Text className="col-span-4 md:col-span-5">{message}</Text>
    </div>
  );
};

export default ComplexPrerequisites;
