import Button from "@/components/common/Button";
import SectionCard from "@/components/common/SectionCard";
import { Heading, Text } from "@chakra-ui/react";

const GeneralInformation = ({ guide, specialty }: { guide: string; specialty: string }) => {
  return (
    <SectionCard header={<Heading textAlign="center">General Information</Heading>}>
      <div className="card p-4">
        <div className="text-center mb-4">
          <Text>
            <span className="font-bold">Study Guide: </span>
            {guide}
          </Text>
          <Text>
            <span className="font-bold">Specialty: </span>
            {specialty}
          </Text>
        </div>
        <div className="flex justify-evenly">
          <Button
            text="Contact"
            icon="/icons/left-exit.svg"
            isRightIcon
            handleClick={() => {
              console.log("Contact - General Information");
            }}
          />
          <Button
            text="Guide"
            icon="/icons/left-exit.svg"
            isRightIcon
            handleClick={() => {
              console.log("Guide - General Information");
            }}
          />
        </div>
      </div>
    </SectionCard>
  );
};

export default GeneralInformation;
