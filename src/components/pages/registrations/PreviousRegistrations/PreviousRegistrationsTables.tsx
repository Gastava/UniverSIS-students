import SectionCard from "@/components/common/SectionCard";
import {
  Divider,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from "@chakra-ui/react";

const semesterEnding = (semester: number) => {
  if (semester === 1) return "st";
  if (semester === 2) return "nd";
  if (semester === 3) return "rd";
  return "th";
};

function RegTableEntry({ regClass }: { regClass: any }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const instructors =
    regClass.courseClass.instructors?.map(
      (instructor: any) => instructor.instructor.givenName + " " + instructor.instructor.familyName
    ) ?? [];

  return (
    <>
      <div onClick={onOpen} className="grid grid-cols-4 p-2">
        <Text className="col-span-2">
          {regClass.course.name} • {regClass.course.displayCode}
        </Text>
        <Text>{regClass.semester + semesterEnding(regClass.semester)}</Text>
        <Text color="green.400">{regClass.ects}</Text>
      </div>

      <Modal returnFocusOnClose={false} preserveScrollBarGap isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{regClass.course.name}</ModalHeader>
          <ModalCloseButton />

          <ModalBody px={0} py={2}>
            <div className="grid gap-4 text-center">
              <Divider />
              <div>
                <Heading size="sm">Instructor</Heading>
                <Text> {instructors.join(", ")}</Text>
              </div>
              <Divider />
              <div>
                <Heading size="sm">Type</Heading>
                <Text> {regClass.courseType.name}</Text>
              </div>
              <Divider />
              <div>
                <Heading size="sm">CP</Heading>
                <Text> {regClass.course.units}</Text>
              </div>
              <Divider />
              <div>
                <Heading size="sm">Hours</Heading>
                <Text> {regClass.hours}</Text>
              </div>
            </div>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

function RegTable({ registration }: { registration: any }) {
  if (registration.classes.length === 0) return null;

  return (
    <SectionCard
      padding={4}
      header={
        <Heading size="lg" textAlign="center">
          {registration.registrationPeriod.name} Semester {registration.registrationYear.name}
        </Heading>
      }
    >
      <div className="card grid grid-cols-1 text-center divide-y">
        <div className="grid grid-cols-4 p-4">
          <Heading className="col-span-2" size="md">
            Subject
          </Heading>
          <Heading size="md">Semester</Heading>
          <Heading size="md">ECTS</Heading>
        </div>
        {registration.classes.map((regClass: any) => (
          <RegTableEntry key={regClass.id} regClass={regClass} />
        ))}
      </div>
    </SectionCard>
  );
}

function PreviousRegistrationsTables({ registrations }: { registrations: any[] }) {
  return (
    <div className="grid gap-4">
      {registrations.map((registration) => (
        <RegTable key={registration.id} registration={registration} />
      ))}
    </div>
  );
}

export default PreviousRegistrationsTables;
