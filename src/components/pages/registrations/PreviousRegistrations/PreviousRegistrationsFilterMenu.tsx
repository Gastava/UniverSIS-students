import { Input, InputGroup, InputRightElement, Select } from "@chakra-ui/react";
import Image from "next/image";

interface RegistrationsFilterMenuProps {
  search: string;
  setSearch: React.Dispatch<React.SetStateAction<string>>;
  selectedPeriod: string;
  setSelectedPeriod: React.Dispatch<React.SetStateAction<string>>;
  allPeriods: Set<string>;
}

function RegistrationsFilterMenu({
  search,
  setSearch,
  selectedPeriod,
  setSelectedPeriod,
  allPeriods,
}: RegistrationsFilterMenuProps) {
  return (
    <div>
      <div className="grid grid-cols-3 gap-4">
        <InputGroup className="col-span-2">
          <Input
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            background="backgroundDark"
            placeholder="Course or Code.."
          />
          <InputRightElement>
            {search ? (
              <Image src="/icons/x.svg" onClick={() => setSearch("")} width={20} height={20} alt="search" />
            ) : (
              <Image src="/icons/search.svg" width={20} height={20} alt="search" />
            )}
          </InputRightElement>
        </InputGroup>
        <Select
          value={selectedPeriod}
          onChange={(e) => setSelectedPeriod(e.target.value)}
          bg="backgroundDark"
          placeholder="All periods"
          color="white"
          sx={{
            "> option": {
              background: "backgroundDark",
              color: "white",
            },
          }}
        >
          {Array.from(allPeriods).map((period) => (
            <option key={period} value={period}>
              {period}
            </option>
          ))}
        </Select>
      </div>
    </div>
  );
}

export default RegistrationsFilterMenu;
