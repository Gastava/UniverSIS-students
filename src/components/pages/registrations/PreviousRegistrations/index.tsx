import SectionCard from "@/components/common/SectionCard";
import useGetAllRegistrations from "@/queries/useGetAllRegistrations";
import { Heading, Text } from "@chakra-ui/react";
import { useState } from "react";
import RegistrationsFilterMenu from "./PreviousRegistrationsFilterMenu";
import PreviousRegistrationsTables from "./PreviousRegistrationsTables";

function PreviousRegistrations() {
  const [search, setSearch] = useState("");
  const [selectedPeriod, setSelectedPeriod] = useState("");
  const { allRegistrations } = useGetAllRegistrations();

  const filteredRegistrationsByPeriod = allRegistrations.filter((registration) => {
    return registration.registrationYear.name == selectedPeriod || selectedPeriod == "";
  });

  const filteredRegistrations = filteredRegistrationsByPeriod.map((registration) => {
    return {
      ...registration,
      classes: registration.classes.filter((regClass: any) => {
        return (
          regClass.course.name.toLowerCase().includes(search.toLowerCase()) ||
          regClass.course.displayCode.toLowerCase().includes(search.toLowerCase())
        );
      }),
    };
  });

  const allPeriods = new Set(allRegistrations.map((registration) => registration.registrationYear.name));

  return (
    <div className="grid gap-4">
      <SectionCard
        padding={4}
        header={
          <Heading size="lg" textAlign="center">
            Previous Registration Periods
          </Heading>
        }
      >
        <div className="card p-4 mb-4">
          <Text align="center">
            The following list contains your courses/ textbook registrations for each academic year.
          </Text>
        </div>
        <RegistrationsFilterMenu
          search={search}
          setSearch={setSearch}
          selectedPeriod={selectedPeriod}
          setSelectedPeriod={setSelectedPeriod}
          allPeriods={allPeriods}
        />
      </SectionCard>
      <PreviousRegistrationsTables registrations={filteredRegistrations} />
    </div>
  );
}

export default PreviousRegistrations;
