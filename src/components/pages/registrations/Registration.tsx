import SectionCard from "@/components/common/SectionCard";
import TotalTracker from "@/components/common/TotalTracker";
import useGetCurrentRegistrationEffectiveStatus from "@/queries/useGetCurrentRegistrationEffectiveStatus";
import useGetUser from "@/queries/useGetUser";
import { Button, Heading, Text } from "@chakra-ui/react";

function NewRegistrations() {
  const { effectiveStatus } = useGetCurrentRegistrationEffectiveStatus();
  const user = useGetUser();

  const registrationPeriodStart = new Date(user?.department?.registrationPeriodStart);
  const registrationPeriodEnd = new Date(user?.department?.registrationPeriodEnd);
  const currentPeriod = user?.department?.currentPeriod?.alternateName;
  const currentYear = user?.department?.currentYear?.alternateName;

  const periodSatistics = [
    { label: "Courses", value: `0` },
    { label: "Hours", value: `0` },
    { label: "ECTS", value: `0` },
  ];

  return (
    <div>
      <SectionCard
        padding={4}
        header={
          <Heading fontWeight="400" size="lg">
            Registration Status ·{" "}
            <span className="text-sm text-nowrap">
              {registrationPeriodStart.toLocaleDateString()} - {registrationPeriodEnd.toLocaleDateString()}
            </span>
          </Heading>
        }
      >
        <div className="card p-4 mb-4">
          <Text align="center" mb="4">
            Good news! Registrations are now open for submission. Go ahead and submit yours!
          </Text>
          <div className="flex justify-around">
            <Button colorScheme="green" px="12">
              <Text>Edit</Text>
            </Button>
            <Button variant="outline" px="12">
              <Text>Print</Text>
            </Button>
          </div>
        </div>
        <TotalTracker showTotalLabel stats={periodSatistics} />
      </SectionCard>
    </div>
  );
}

export default NewRegistrations;
