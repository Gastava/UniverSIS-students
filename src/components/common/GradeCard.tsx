import useGetGradeStatistics from "@/queries/useGetGradeStatistics";
import { Box, Flex, Heading, IconButton, Text } from "@chakra-ui/react";
import Image from "next/image";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Cell,
  ResponsiveContainer,
} from "recharts";

interface GradeCardProps {
  title: string;
  code: string;
  teachers?: string[];
  grade: number;
  isPassed?: boolean;
  courseExamId: string | null;
  drawer?: React.ReactNode;
}

const renderCustomBarLabel = ({
  x,
  y,
  width,
  value,
}: {
  x: number;
  y: number;
  width: number;
  value: number;
}) => {
  return (
    <text
      x={x + width / 2}
      y={y}
      fill="#666"
      textAnchor="middle"
      dy={-5}
      fontSize={10}
    >
      {value}
    </text>
  );
};

const isGradeInRange = (range: string, grade: number) => {
  const [min, max] = range.split("-").map(Number);
  return grade >= min && grade < max;
};

const maxTotal = (data: { name: string; total: number }[]): number => {
  const max = Math.max(...data.map((d) => d.total));
  return max + 10;
};

const GradeCardStatistics = ({
  courseExamId,
  grade,
}: {
  courseExamId: string;
  grade: number;
}) => {
  const { t } = useTranslation("grades");
  const { average, averagePass, graded, passing, brackets } =
    useGetGradeStatistics(courseExamId);

  const data = brackets.map((bracket) => ({
    name: `${bracket.from}-${bracket.to}`,
    total: bracket.total,
  }));

  const maxTotalValue = maxTotal(data);

  return (
    <div className="grid w-full h-full">
      <ResponsiveContainer>
        <BarChart data={data}>
          <XAxis dataKey="name" tick={{ fontSize: "10px" }} height={20} />
          <YAxis tick={false} width={1} domain={[0, maxTotalValue]} />

          <Bar dataKey="total" label={renderCustomBarLabel}>
            {data.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={isGradeInRange(entry.name, grade) ? "#48BB78" : "#A0AEC0"}
              />
            ))}
          </Bar>
        </BarChart>
      </ResponsiveContainer>

      <div className="grid grid-cols-2">
        <Text variant={"cardSubtitle"}>
          {t("Grades.Chart.Graded")} : {graded}
        </Text>
        <Text variant={"cardSubtitle"}>
          {t("Grades.Chart.Successful")} : {passing}
        </Text>
        <Text variant={"cardSubtitle"}>
          {t("Grades.Chart.AverageGrade")} : {average}
        </Text>
        <Text variant={"cardSubtitle"}>
          {t("Grades.Chart.AveragePassingGrade")} : {averagePass}
        </Text>
      </div>
    </div>
  );
};

const GradeCard = ({
  title,
  code,
  teachers,
  grade,
  isPassed = false,
  courseExamId,
  drawer,
}: GradeCardProps) => {
  const [flipped, setFlipped] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen((prev) => !prev);
  };

  const flipCard = () => {
    setFlipped((prev) => !prev);
  };

  return (
    <div className="card">
      <div className="h-32 flex gap-2 pr-2">
        <Flex
          flexGrow={1}
          justifyContent="space-between"
          borderRight="1px solid #718096"
          alignItems={!flipped ? "center" : "flex-end"}
          p={4}
        >
          {!flipped ? (
            <>
              <Box>
                <Heading fontSize="sm">{title.toLocaleUpperCase()}</Heading>
                <Text variant="cardSubtitle">
                  {code} {teachers && " • " + teachers.join(", ")}
                </Text>
              </Box>
              <Text fontSize="3xl" color={isPassed ? "green.400" : "gray.400"}>
                {grade}
              </Text>{" "}
            </>
          ) : (
            courseExamId && (
              <GradeCardStatistics courseExamId={courseExamId} grade={grade} />
            )
          )}
        </Flex>
        <IconButton
          aria-label="statistics"
          height="100%"
          backgroundColor="transparent"
          _hover={{ backgroundColor: "transparent" }}
          _active={{ backgroundColor: "transparent" }}
          _focus={{ backgroundColor: "transparent" }}
          onClick={flipCard}
          icon={
            <Image
              alt={flipped ? "statistics" : "grade"}
              src={flipped ? "/icons/star.svg" : "/icons/graph.svg"}
              width={20}
              height={20}
            />
          }
        />
      </div>
      {drawer && (
        <div className="border-t border-[#718096]">
          <div className="flex justify-center">
            <IconButton
              aria-label="more info"
              backgroundColor="transparent"
              _active={{ backgroundColor: "transparent" }}
              _focus={{ backgroundColor: "transparent" }}
              onClick={toggleDrawer}
              icon={
                <Image
                  alt={drawerOpen ? "close" : "open"}
                  src={`/icons/arrow-${drawerOpen ? "up" : "down"}.svg`}
                  width={24}
                  height={19}
                />
              }
            />
          </div>
          {drawerOpen && <div className="p-4">{drawer}</div>}
        </div>
      )}
    </div>
  );
};

export default GradeCard;
