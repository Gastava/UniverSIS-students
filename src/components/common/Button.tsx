import { Button as ChakraButton } from "@chakra-ui/react";
import Image from "next/image";

function Button({
  text,
  icon,
  handleClick,
  chakraProps,
  isRightIcon = false,
}: {
  text: string;
  handleClick: () => void;
  icon?: string;
  isRightIcon?: boolean;
  chakraProps?: any;
}) {
  return (
    <ChakraButton
      variant="outline"
      _hover={{}}
      color={"text"}
      {...chakraProps}
      onClick={handleClick}
      leftIcon={icon && !isRightIcon && <Image src={icon} alt={text} width={14} height={14} />}
      rightIcon={icon && isRightIcon && <Image src={icon} alt={text} width={14} height={14} />}
    >
      {text}
    </ChakraButton>
  );
}

export default Button;
