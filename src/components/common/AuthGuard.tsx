import { useApplicationContext } from "@/context/ApplicationContext";
import { useAuth } from "@/context/AuthContext";
import { useRouter } from "next/router";
import { useEffect } from "react";

// TODO: change from implicit flow to PCKE
const AuthGuard = ({ children }: { children: React.ReactNode }) => {
  const { user, setUser } = useAuth();
  const { configuration, theMostClient } = useApplicationContext();
  const router = useRouter();

  useEffect(() => {
    const {
      authorizeURL,
      oauth2: { clientID, scope, callbackURL },
    } = configuration.settings.auth;

    async function tryUser(access_token: string) {
      theMostClient.setBearerAuthorization(access_token);
      const userResponse = await theMostClient.model("Users/Me").asQueryable().expand("groups").getItem();
      if (userResponse && setUser) {
        setUser({ ...userResponse, access_token: access_token });

        window.history.pushState("", document.title, window.location.pathname + window.location.search);
      }
    }

    if (window.location.hash && !user) {
      const hashParams = new URLSearchParams(window.location.hash.substring(1));
      const accessToken = hashParams.get("access_token");
      if (accessToken) tryUser(accessToken);
    } else if (!user) {
      const loginUrl = `${authorizeURL}?client_id=${clientID}&response_type=token&scope=${scope.join(
        ","
      )}&redirect_uri=${callbackURL}`;

      window.location.href = loginUrl;
    }
  }, [user, setUser, router, theMostClient, configuration.settings.auth]);

  if (!user) {
    return <div>Loading...</div>;
  }

  return <>{children}</>;
};

export default AuthGuard;
