import { Box, Button, Flex, Heading, Text } from "@chakra-ui/react";
import Card from "./Card";

interface CourseCardProps {
  title: string;
  code: string;
  teachers: string[];
}

const CourseCard = ({ title, code, teachers }: CourseCardProps) => {
  return (
    <Card>
      <Flex height="100%" alignItems={"center"} p={4}>
        <Box flexGrow={1}>
          <Heading fontSize="sm">{title.toLocaleUpperCase()}</Heading>
          <Text variant="cardSubtitle">
            {code} • {teachers.join(", ")}
          </Text>
        </Box>
        <Button size="sm" colorScheme="green" variant="outline" flexShrink={0}>
          eLearning
        </Button>
      </Flex>
    </Card>
  );
};

export default CourseCard;
