import React from "react";

const Card = ({ children }: { children: React.ReactNode }) => {
  return <div className="bg-backgroundDark rounded-xl h-fit">{children}</div>;
};

export default Card;
