import { Text } from "@chakra-ui/react";

interface TotalTrackerProps {
  stats: { label: string; value: string }[];
  showTotalLabel?: boolean;
}

function TotalTracker({ stats, showTotalLabel = false }: TotalTrackerProps) {
  return (
    <div className=" bg-backgroundDark rounded-xl flex justify-around p-2">
      {showTotalLabel && <Text>Total:</Text>}
      {stats.map((stat) => {
        return (
          <div key={stat.label} className="flex gap-2">
            <Text color="green.400">{stat.value}</Text>
            <Text>{stat.label}</Text>
          </div>
        );
      })}
    </div>
  );
}

export default TotalTracker;
