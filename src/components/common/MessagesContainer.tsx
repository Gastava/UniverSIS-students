import { Card, CardHeader, Text, Heading, CardBody } from "@chakra-ui/react";

const MessagesContainer = ({
  title,
  numberOfMessages,
  children,
}: {
  title: string;
  numberOfMessages: number;
  children: React.ReactNode;
}) => {
  return (
    <Card
      bg={"#1A202C"}
      bgGradient="radial(rgba(26, 32, 44, 0.1), rgba(190, 227, 248, 0.1))"
      border="1px solid #FFFFFFCC"
    >
      <CardHeader>
        <div className="flex items-center justify-center gap-2">
          <Text fontSize="xl" color="green.400">
            {numberOfMessages}
          </Text>
          <Heading flexShrink={0} size="md">
            {title}
          </Heading>
        </div>
      </CardHeader>
      <div className="border-b border-gray-500" />
      <CardBody>{children}</CardBody>
    </Card>
  );
};

export default MessagesContainer;
