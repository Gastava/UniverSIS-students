import React, { useState } from "react";
import { Box, IconButton, Image, Text } from "@chakra-ui/react";

const MessageCard = ({
  read = false,
  children,
  drawerContent,
}: {
  read: boolean;
  children: React.ReactNode;
  drawerContent: React.ReactNode;
}) => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen(!drawerOpen);
  };

  return (
    <div className={`${read ? "bg-gray-400" : "bg-green-400"} rounded-lg mb-2`}>
      <div
        className={`flex ${
          drawerContent ? "flex-col" : ""
        } items-center gap-2 bg-backgroundDark rounded-lg p-3 ml-1`}
      >
        {children}
        {drawerContent && (
          <div className="flex relative w-full justify-center mt-2">
            <div
              className={`flex flex-col items-center ${
                drawerOpen ? "absolute bottom-0 w-full" : "w-full"
              }`}
            >
              <div className="border-b border-gray-500 w-full" />
              <IconButton
                height={6}
                width={6}
                aria-label="more info"
                backgroundColor="transparent"
                _hover={{}}
                _active={{}}
                onClick={toggleDrawer}
                icon={
                  <Image
                    alt={drawerOpen ? "close" : "open"}
                    src={`/icons/arrow-${drawerOpen ? "up" : "down"}.svg`}
                    width={"full"}
                    height={"full"}
                  />
                }
              />
            </div>
            {drawerOpen && <div className="mb-10">{drawerContent}</div>}
          </div>
        )}
      </div>
    </div>
  );
};

export default MessageCard;
