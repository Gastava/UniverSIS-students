import i18n from "@/config/i18n";
import { Divider, Heading, Text } from "@chakra-ui/react";
import { AnimatePresence, motion } from "framer-motion";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "./Button";
import NotificationPopup from "./NotificationPopup";
import { useGetAllUnreadMessages } from "@/queries/useGetMessages";

const NavItem = ({
  icon,
  text,
  url,
}: {
  icon: string;
  text: string;
  url: string;
}) => {
  const router = useRouter();
  const path = router.pathname;

  const isActive = path === url;

  return (
    <Link href={url}>
      <div
        className={`h-full py-2 px-1 ${
          isActive && "bg-white/20"
        } md:p-4 md:flex md:gap-2`}
      >
        <div className="flex items-center justify-center mb-1 md:mb-0">
          <Image
            src={`/icons/${icon}${!isActive ? "-dark" : ""}.svg`}
            alt="dashboard"
            width={32}
            height={32}
          />
        </div>
        <Heading size="xs" alignContent="center" textAlign="center">
          {text}
        </Heading>
      </div>
    </Link>
  );
};

const SidebarItem = ({
  icon,
  text,
  onClick,
}: {
  icon: string;
  text: string;
  onClick?: () => void;
}) => {
  return (
    <div className="flex gap-4 cursor-pointer" onClick={onClick}>
      <Image src={icon} alt={text} width={24} height={24} />
      <Text>{text}</Text>
    </div>
  );
};

const SideNav = ({
  handleClose,
  mobile = false,
}: {
  handleClose?: () => void;
  mobile?: boolean;
}) => {
  const { t } = useTranslation("common");

  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language);
  };

  return (
    <div className="bg-[#1A202C] px-4 py-8 col-span-3 h-screen">
      <div className="grid gap-4">
        <div className="flex flex-col items-center gap-1">
          <Image src="/img/avatar.png" alt="avatar" width={74} height={74} />
          <Text>Stefanos Giannakos</Text>
        </div>
        <Divider />
        <Heading size="sm" pl="10">
          {t("Student")}
        </Heading>

        <Link
          href="/about"
          onClick={() => mobile && handleClose && handleClose()}
        >
          <SidebarItem icon="/icons/person.svg" text={t("ProfileTitle")} />
        </Link>
        <Link
          href="/graduation/rules"
          onClick={() => mobile && handleClose && handleClose()}
        >
          <SidebarItem icon="/icons/graduation.svg" text={t("Graduation")} />
        </Link>
        <Divider />
        <Heading size="sm" pl="10">
          {t("Support")}
        </Heading>
        <Link
          href="/requests"
          onClick={() => mobile && handleClose && handleClose()}
        >
          <SidebarItem icon="/icons/requests.svg" text={t("Requests")} />
        </Link>
        <Link
          href="/help"
          onClick={() => mobile && handleClose && handleClose()}
        >
          <SidebarItem icon="/icons/help.svg" text={t("Help")} />
        </Link>
        <Link
          href="/benefits"
          onClick={() => mobile && handleClose && handleClose()}
        >
          <SidebarItem icon="/icons/benefits.svg" text={t("Benefits")} />
        </Link>
        <Divider />
        <Heading size="sm" pl="10">
          {t("Preferences")}
        </Heading>
        <SidebarItem icon="/icons/moon.svg" text={t("Theme")} />
        <SidebarItem
          icon="icons/language.svg"
          text={t("Language")}
          onClick={() => {
            const selectedLanguage = i18n.language === "en" ? "el" : "en"; // Toggle between 'en' and 'el' languages
            changeLanguage(selectedLanguage);
          }}
        />
        <div className="flex justify-center">
          <Button
            text={t("Logout")}
            icon="/icons/left-exit.svg"
            handleClick={() => {
              console.log("logout redirect");
            }}
          />
        </div>
      </div>
    </div>
  );
};

const DesktopSideNav = () => {
  return (
    <motion.div
      whileHover={{ width: 256 }}
      className="md:absolute z-40 h-screen w-14 overflow-hidden border-whitecc border-r hidden md:block"
    >
      <div className="w-64">
        <SideNav />
      </div>
    </motion.div>
  );
};

const MobileSideNav = ({ handleClose }: { handleClose: () => void }) => {
  return (
    <div className="fixed left-0 h-screen w-full z-50 md:hidden grid grid-cols-5">
      <div onClick={handleClose} className="col-span-2"></div>
      <motion.div
        initial={{ x: "100%" }}
        animate={{ x: 0 }}
        exit={{ x: "100%" }}
        className="rounded-l-3xl col-span-3 overflow-hidden"
      >
        <SideNav handleClose={handleClose} mobile />
      </motion.div>
    </div>
  );
};

const MainNavbar = ({
  setMobileSideNavOpen,
}: {
  setMobileSideNavOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const { t } = useTranslation("common");
  const [showPopup, setShowPopup] = useState(false);
  const [showMobilePopup, setShowMobilePopup] = useState(false);

  const allUnreadMessages = useGetAllUnreadMessages();
  const hasUnreadNotifications = allUnreadMessages.length > 0;

  const toggleSideNav = () => {
    setMobileSideNavOpen((prev) => !prev);
  };

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const toogleMobilePopup = () => {
    setShowMobilePopup(!showMobilePopup);
  };

  return (
    <div className="border-b border-whitecc bg-[#171923] flex justify-between">
      <div className="flex w-1/4 justify-between">
        <div className="flex gap-4 p-4 z-50">
          <Image
            src="/icons/logo-universis.svg"
            alt="Universis"
            width={38}
            height={38}
          />
          <Heading size="lg" alignContent="center">
            {t("Dashboard")}
          </Heading>
        </div>
        <div className="flex gap-2 hidden md:flex">
          <Divider orientation="vertical" className="h-full"></Divider>
          <Image
            src="/icons/bell.svg"
            alt="notifications"
            width={32}
            height={32}
            onClick={togglePopup}
          />
          {showPopup && (
            <NotificationPopup onClose={() => setShowPopup(false)} />
          )}
          {/* Render popup if showPopup is true */}
        </div>
      </div>

      <div className="grid grid-cols-4 fixed bottom-0 z-50 w-full bg-[#171923] border-t border-whitecc rounded-t-md md:static md:flex md:border-none md:w-fit">
        <NavItem icon="dashboard" text={t("Dashboard")} url="/" />
        <NavItem icon="nav-star" text={t("Grades")} url="/grades" />
        <NavItem icon="grid" text={t("registrations")} url="/registrations" />
        <NavItem icon="schedule" text={t("Schedule")} url="/schedule" />
      </div>
      <div className="flex md:hidden p-4 gap-4">
        <div className="m-auto">
          <Image
            src="/icons/bell.svg"
            alt="notifications"
            width={32}
            height={32}
            onClick={toogleMobilePopup}
          />
          {hasUnreadNotifications && (
            <div className="absolute top-0 right-0 bg-blue-500 text-white text-xs rounded-full w-3 h-3 flex items-center justify-center">
              {allUnreadMessages.length}
            </div>
          )}
        </div>
        {showMobilePopup && (
          <NotificationPopup onClose={() => setShowMobilePopup(false)} />
        )}
        <div className="m-auto" onClick={toggleSideNav}>
          <Image
            src="/icons/hamburger.svg"
            alt="sidebar-toggle"
            width={32}
            height={32}
          />
        </div>
      </div>
    </div>
  );
};

export default function Navbar() {
  const [mobileSideNavOpen, setMobileSideNavOpen] = useState(false);
  return (
    <div>
      <MainNavbar setMobileSideNavOpen={setMobileSideNavOpen} />
      <AnimatePresence>
        {mobileSideNavOpen && (
          <MobileSideNav handleClose={() => setMobileSideNavOpen(false)} />
        )}
      </AnimatePresence>
      <DesktopSideNav />
    </div>
  );
}
