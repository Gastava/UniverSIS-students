import React from "react";
import { Text, Image, Button } from "@chakra-ui/react";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import MessageCard from "./MessageCard";
import { useGetAllUnreadMessages } from "@/queries/useGetMessages";

interface NotificationPopupProps {
  onClose: () => void;
}

const NotificationPopup = ({ onClose }: NotificationPopupProps) => {
  const { t } = useTranslation("messages");
  const allUnreadMessages = useGetAllUnreadMessages();

  return (
    <div className="absolute right-0 mt-[53px] w-full md:left-[23%] md:mt-[71px] md:w-1/3 bg-backgroundDark rounded-b-2xl">
      <div className="p-4 flex items-center justify-between">
        <Text fontSize="lg">{t("Messages.Notifications")}</Text>
        <Link href="/messages">
          <Button
            size="sm"
            colorScheme="green"
            variant="outline"
            _hover={{}}
            _active={{}}
          >
            {t("Messages.AllMessages")}
          </Button>
        </Link>
      </div>
      <div className="border-b border-gray-500" />
      <div className="p-2">
        {allUnreadMessages.length === 0 ? (
          <Text textAlign="center" color="gray.400">
            {t("Messages.NoNewMessages")}
          </Text>
        ) : (
          allUnreadMessages.map((message, index) => (
            <MessageCard
              key={index}
              read={!message.dateReceived}
              drawerContent={""}
            >
              <Image
                src="/icons/envelope.svg"
                alt="envelope-icon"
                width={5}
                height={5}
              />
              <Text>{message.subject}</Text>
            </MessageCard>
          ))
        )}
      </div>
      <div className="border-b border-gray-500 mb-3 " />
      <div onClick={onClose} className="flex items-center p-2">
        <div className="bg-white h-0.5 w-1/5 mx-auto "></div>
      </div>
    </div>
  );
};

export default NotificationPopup;
