import { Progress, Text } from "@chakra-ui/react";

function PeriodProgress({ passed, total }: { passed: number; total: number }) {
  return (
    <div className="relative flex-grow">
      <Progress
        value={Math.round((passed / total) * 100)}
        size="lg"
        colorScheme="green"
        borderRadius="full"
        backgroundColor="gray.400"
      />
      <Text position="absolute" top="50%" left="50%" transform="translate(-50%, -50%)">
        {Math.round((passed / total) * 100)}%
      </Text>
    </div>
  );
}

export default PeriodProgress;
