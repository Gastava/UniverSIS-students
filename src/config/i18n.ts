import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
//* TRANSLATION EL
import translationEL from '@/locales/el/el.json';
import translationDashboardEL from "@/locales/el/dashboard.el.json"
import translationGradesEL from "@/locales/el/grades.el.json"
import translationExamPeriodEL from "@/locales/el/exam-period-participate-action.el.json"
import translationInfoEL from "@/locales/el/info.el.json"
import translationMessagesEL from "@/locales/el/messages.el.json"
import translationParticipatePeriodEL from "@/locales/el/period-participate-request-action.el.json"
import translationPreferredSpecialityEL from "@/locales/el/preferred-specialty-request-action.el.json"
import translationProfileEL from "@/locales/el/profile.el.json"
import translationRegistrationsEL from "@/locales/el/registrations.el.json"
import translationRemovalRequestEL from "@/locales/el/removal-request-action.el.json"
import translationRequestEL from "@/locales/el/requests.el.json"
import translationStudenReGradeEL from "@/locales/el/student-grade-remark-action.el.json"
import translationSuspendRequestEL from "@/locales/el/suspend-request-action.el.json"
import translationTeachingEventsEL from "@/locales/el/teaching-events.el.json"
import translationThesisRequestEL from "@/locales/el/thesis-request-action.el.json"

//* TRANSLATION EN
import translationEN from '@/locales/en/en.json'; 
import translationDashboardEN from "@/locales/en/dashboard.en.json"
import translationGradesEN from "@/locales/en/grades.en.json"
import translationExamPeriodEN from "@/locales/en/exam-period-participate-action.en.json"
import translationInfoEN from "@/locales/en/info.en.json"
import translationMessagesEN from "@/locales/en/messages.en.json"
import translationParticipatePeriodEN from "@/locales/en/period-participate-request-action.en.json"
import translationPreferredSpecialityEN from "@/locales/en/preferred-specialty-request-action.en.json"
import translationProfileEN from "@/locales/en/profile.en.json"
import translationRegistrationsEN from "@/locales/en/registrations.en.json"
import translationRemovalRequestEN from "@/locales/en/removal-request-action.en.json"
import translationRequestEN from "@/locales/en/requests.en.json"
import translationStudenReGradeEN from "@/locales/en/student-grade-remark-action.en.json"
import translationSuspendRequestEN from "@/locales/en/suspend-request-action.en.json"
import translationTeachingEventsEN from "@/locales/en/teaching-events.en.json"
import translationThesisRequestEN from "@/locales/en/thesis-request-action.en.json"




const resources = {
  el: { 
    common: translationEL,
    dashboard: translationDashboardEL,
    grades: translationGradesEL,
    examPeriod: translationExamPeriodEL,
    info: translationInfoEL,
    messages: translationMessagesEL,
    participatePeriod: translationParticipatePeriodEL,
    preferredSpeciality: translationPreferredSpecialityEL,
    profile: translationProfileEL,
    registration: translationRegistrationsEL,
    removalRequest: translationRemovalRequestEL,
    request: translationRequestEL,
    studentReGrade: translationStudenReGradeEL,
    suspendRequest: translationSuspendRequestEL,
    teachingEvent: translationTeachingEventsEL,
    thesisRequest: translationThesisRequestEL,
  },
  en: {
    common: translationEN,
    dashboard: translationDashboardEN,
    grades: translationGradesEN,
    examPeriod: translationExamPeriodEN,
    info: translationInfoEN,
    messages: translationMessagesEN,
    participatePeriod: translationParticipatePeriodEN,
    preferredSpeciality: translationPreferredSpecialityEN,
    profile: translationProfileEN,
    registration: translationRegistrationsEN,
    removalRequest: translationRemovalRequestEN,
    request: translationRequestEN,
    studentReGrade: translationStudenReGradeEN,
    suspendRequest: translationSuspendRequestEN,
    teachingEvent: translationTeachingEventsEN,
    thesisRequest: translationThesisRequestEN,
  },
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: 'en', 
    debug: false, 
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;