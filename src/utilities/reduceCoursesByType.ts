export function reduceCoursesByType(allCourses: any, type: "semester" | "courseType") {
  return allCourses
    .reduce((acc: any, grade: any) => {
      const itemIncluded = acc.some((item: any) => item.id === grade[type].id);
      if (!itemIncluded) {
        acc.push(grade[type]);
      }
      return acc;
    }, [])
    .sort((a: any, b: any) => {
      return a.id - b.id;
    });
}
