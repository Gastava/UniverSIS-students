import { GradeStatistics } from "@/queries/useGetGradeStatistics";

export function bracketGradesWithNumericScale(gradesDistribution: GradeStatistics[], scaleFactor: number) {
  const numberOfBrackets = 1 / scaleFactor;
  const bracketedGrades = new Array(numberOfBrackets).fill(0);

  gradesDistribution.forEach((grade) => {
    // todo: maybe switch to a better rounding method
    const bracket = Math.floor(+(grade.examGrade / scaleFactor).toFixed(6));
    bracketedGrades[bracket] += grade.total;
  });

  return { bracketedGrades, numberOfBrackets };
}
