import PreviousRegistrations from "@/components/pages/registrations/PreviousRegistrations";
import NewRegistrations from "@/components/pages/registrations/Registration";
import TextbookRegistrations from "@/components/pages/registrations/TextbookRegistrations";
import { Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";

function Registrations() {
  return (
    <Tabs variant="unstyled" isLazy>
      <TabPanels>
        <TabPanel>
          <NewRegistrations />
        </TabPanel>
        <TabPanel>
          <PreviousRegistrations />
        </TabPanel>
        <TabPanel>
          <TextbookRegistrations />
        </TabPanel>
      </TabPanels>

      <TabList
        position="fixed"
        bottom="80px"
        left="50%"
        transform="translateX(-50%)"
        display="flex"
        alignItems="center"
        borderRadius="lg"
        overflow="hidden"
        bg={"backgroundDark"}
        border="1px solid #FFFFFFCC"
      >
        <Tab
          _selected={{ bg: "border", color: "white" }}
          _notLast={{ borderRight: "1px solid", borderColor: "gray.300" }}
          width={120}
          py="3"
        >
          New
        </Tab>
        <Tab
          _selected={{ bg: "border", color: "white" }}
          _notLast={{ borderRight: "1px solid", borderColor: "gray.300" }}
          width={120}
          py="3"
        >
          Previous
        </Tab>
        <Tab _selected={{ bg: "border", color: "white" }} width={120} py="3">
          Textbooks
        </Tab>
      </TabList>
    </Tabs>
  );
}

export default Registrations;
