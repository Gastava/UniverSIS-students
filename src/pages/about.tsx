import SectionCard from "@/components/common/SectionCard";
import useGetUser from "@/queries/useGetUser";
import { Heading, Text } from "@chakra-ui/react";
import Image from "next/image";

function HeaderCard({ name, department }: { name: string; department: string }) {
  return (
    <SectionCard>
      <div className="card grid grid-cols-2">
        <div className="flex justify-center items-center md:gap-8 px-2 py-8 border-r">
          <div className="flex-shrink-0">
            <Image src="/img/avatar.png" alt="avatar" width={48} height={48} />
          </div>
          <Text textAlign="center" fontSize="sm">
            {name}
          </Text>
        </div>
        <div className="flex justify-center items-center md:gap-8 px-2 py-8">
          <div className="flex-shrink-0">
            <Image src="/icons/department.svg" alt="avatar" width={48} height={48} />
          </div>
          <Text textAlign="center" fontSize="sm">
            {department}
          </Text>
        </div>
      </div>
    </SectionCard>
  );
}

function About() {
  const user = useGetUser();
  return (
    <div className="grid gap-4 md:gap-6">
      <HeaderCard name={user?.person?.name} department={user?.department?.name} />
      <SectionCard header={<Heading textAlign="center">My profile</Heading>}>
        <div className="grid md:grid-cols-2 gap-4">
          <div className="md:col-span-2 card">
            <Heading size="lg" textAlign="center" p="2">
              {user?.person?.name}
            </Heading>
          </div>
          <div className="card p-4">
            <Text>
              Username: <span>{user?.user?.name}</span>
            </Text>
            <Text>
              Email: <span>{user?.person?.email}</span>
            </Text>
            <Text>
              Alt Email: <span>{user?.person?.alternateEmail}</span>
            </Text>
            <Text>
              Status: <span>{user?.category?.name}</span>
            </Text>
          </div>
          <div className="card p-4">
            <Text>
              Home Phone: <span>{user?.person?.homePhone}</span>
            </Text>
            <Text>
              Home Address: <span>{user?.person?.homeAddress}</span>
            </Text>
            <Text>
              Personal Phone: <span>{user?.person?.mobilePhone}</span>
            </Text>
            <Text>
              Temporary Address: <span>{user?.person?.temporaryAddress}</span>
            </Text>
          </div>
          <div className="card p-4">
            <Text>
              Speciality: <span>{user?.specialty}</span>
            </Text>
            <Text>
              Curriculum: <span>{user?.studyProgram?.printName}</span>
            </Text>
            <Text>
              Uni ID: <span>{user?.uniqueIdentifier}</span>
            </Text>
            <Text>
              Depar. ID: <span>{user?.studentIdentifier}</span>
            </Text>
          </div>
          <div className="card p-4">
            <Text>
              Inscription Mode: <span>{user?.inscriptionMode?.name}</span>
            </Text>
            <Text>
              Inscription Year: <span>{user?.inscriptionYear?.name}</span>
            </Text>
            <Text>
              Inscription Period: <span>{user?.inscriptionPeriod?.name}</span>
            </Text>
            <Text>
              Inscription Semester: <span>{user?.inscriptionSemester?.name}</span>
            </Text>
          </div>
        </div>
      </SectionCard>
      <SectionCard header={<Heading textAlign="center">My Department</Heading>}>
        <div className="grid md:grid-cols-2 gap-4">
          <div className="md:col-span-2 card">
            <Heading size="lg" textAlign="center" p="2">
              Aristotle University of Thessaloniki
            </Heading>
          </div>
          <div className="card p-4">
            <Text>
              Department: <span>{user?.department?.name}</span>
            </Text>
            <Text>
              Faculty: <span>{user?.department?.facultyName}</span>
            </Text>
            <Text>
              Link: <span>{user?.department?.url}</span>
            </Text>
          </div>
          <div className="card p-4">
            <Text>
              Address: <span>{user?.department?.address}</span>
            </Text>
            <Text>
              City: <span>{user?.department?.city}</span>
            </Text>
            <Text>
              Postal Code: <span>{user?.department?.postalCode}</span>
            </Text>
          </div>
          <div className="card p-4">
            <Text>
              Phone Number: <span>{user?.department?.phone1}</span>
            </Text>
            <Text>
              Email: <span>-</span>
            </Text>
            <Text>
              Alt. Phone Number: <span>{user?.department?.phone2}</span>
            </Text>
          </div>
          <div className="card p-4 h-full">
            <Text>
              Contact Person: <span>{user?.department?.contactPerson1}</span>
            </Text>
            <Text>
              Alt. Cont. Person: <span>{user?.department?.contactPerson2}</span>
            </Text>
          </div>
        </div>
      </SectionCard>
    </div>
  );
}

export default About;

// two column design

{
  /* <div className="card grid grid-cols-2">
  <div className="grid gap-2 p-4 border-r border-border">
    <Text>
      Username: <span>Stefanos</span>
    </Text>
    <Text>
      Username: <span>Stefanos</span>
    </Text>
  </div>
  <div className="grid p-4">
    <Text>
      Username: <span>Stefanos</span>
    </Text>
    <Text>
      Username: <span>Stefanos</span>
    </Text>
  </div>
</div>; */
}
