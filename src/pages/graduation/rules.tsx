import ApplyGraduation from "@/components/pages/graduation/rules/ApplyForGraduationCard";
import ComplexPrerequisites from "@/components/pages/graduation/rules/ComplexPrerequisites";
import GeneralInformation from "@/components/pages/graduation/rules/GeneralInformation";
import useGetGraduationRules from "@/queries/useGetGraduationRules";
import useGetUser from "@/queries/useGetUser";

export default function GraduationRules() {
  const user = useGetUser();
  const graduationRules = useGetGraduationRules();
  return (
    <div>
      <div className="grid md:grid-cols-2 gap-8 mb-8">
        <GeneralInformation guide={user?.studyProgram.name} specialty={user?.specialty} />
        <ApplyGraduation successful={graduationRules?.finalResult?.success} />
      </div>
      <ComplexPrerequisites graduationRules={graduationRules?.graduationRules} />
    </div>
  );
}
