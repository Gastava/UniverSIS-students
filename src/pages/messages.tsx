import AllMessages from "@/components/pages/notification/AllMessages";
export default function Notification() {
  return (
    <div className="grid gap-4 sm:gap-6">
      <AllMessages />
    </div>
  );
}
