// import MyCourses from "@/components/common/MyCourses";
import HeaderCards from "@/components/pages/dashboard/headerCards";
import MyCourses from "@/components/pages/dashboard/MyCourses";
import RecentGrades from "@/components/pages/dashboard/RecentGrades";

export default function Dashboard() {
  return (
    <div className="grid gap-4 sm:gap-6">
      <HeaderCards />
      <RecentGrades />
      <MyCourses />
    </div>
  );
}
