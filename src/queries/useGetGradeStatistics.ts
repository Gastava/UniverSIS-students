import { useTheMostClientContext } from "@/context/ApplicationContext";
import { examStatisticsMock } from "@/queries/mocks";
import { bracketGradesWithNumericScale } from "@/utilities/bracketGradesWithScale";
import { useEffect, useState } from "react";

export interface GradeStatistics {
  examGrade: number;
  isPassed: number;
  total: number;
}

export default function useGetGradeStatistics(courseExamId: string) {
  const theMostClient = useTheMostClientContext();

  const [gradeStatistics, setGradeStatistics] = useState<GradeStatistics[]>([]);

  // todo: get the actual scale factor and value ranges
  // ! ask about grade scales
  const scaleFactor = 0.1;
  const valueFrom = 10;
  const valueTo = 0;

  useEffect(() => {
    let ignore = false;

    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setGradeStatistics(examStatisticsMock);
    } else {
      theMostClient
        .model(`/students/me/exams/${courseExamId}/statistics`)
        .asQueryable()
        .take(-1)
        .getItems()
        .then((res) => {
          if (!ignore) {
            setGradeStatistics(res);
          }
        });
    }

    return () => {
      ignore = true;
    };
  }, [courseExamId, theMostClient]);

  const statistics = gradeStatistics.reduce(
    (acc, curr) => {
      acc.graded += curr.total;
      acc.gradesSum += curr.examGrade * curr.total;
      if (curr.isPassed === 1) {
        acc.passing += curr.total;
        acc.gradesPassSum += curr.examGrade * curr.total;
      }
      return acc;
    },
    { graded: 0, passing: 0, gradesSum: 0, gradesPassSum: 0 }
  );

  const { bracketedGrades, numberOfBrackets } = bracketGradesWithNumericScale(gradeStatistics, scaleFactor);

  // ? maybe move to bracketGradesWithNumericScale
  const brackets = new Array(numberOfBrackets).fill(0).map((_, i) => {
    return {
      from: (i / numberOfBrackets) * (valueFrom - valueTo) + valueTo,
      to: ((i + 1) / numberOfBrackets) * (valueFrom - valueTo) + valueTo,
      total: bracketedGrades[i],
    };
  });

  return {
    graded: statistics.graded,
    passing: statistics.passing,
    average: +(statistics.gradesSum / statistics.graded).toFixed(2),
    averagePass: +(statistics.gradesPassSum / statistics.passing).toFixed(2),
    brackets,
  };
}
