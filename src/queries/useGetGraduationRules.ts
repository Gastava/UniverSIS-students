import { useTheMostClientContext } from "@/context/ApplicationContext";
import { useEffect, useState } from "react";
import { graduationRulesMock } from "./mocks";

function useGetGraduationRules() {
  const theMostClient = useTheMostClientContext();
  const [rules, setRules] = useState<any>();

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setRules(graduationRulesMock);
    } else {
      theMostClient
        .model("students/me/graduationRules")
        .asQueryable()
        .expand("validationResult")
        .take(-1)
        .getItems()
        .then((rules) => {
          setRules(rules);
        });
    }
  }, [theMostClient]);

  return rules;
}

export default useGetGraduationRules;
