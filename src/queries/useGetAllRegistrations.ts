import { useTheMostClientContext } from "@/context/ApplicationContext";
import { allRegistrationsMock } from "@/queries/mocks";
import { useEffect, useState } from "react";

export default function useGetAllRegistrations() {
  const theMostClient = useTheMostClientContext();

  const [allRegistrations, setAllRegistrations] = useState<any[]>([]);

  useEffect(() => {
    let ignore = false;

    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setAllRegistrations(allRegistrationsMock);
    } else {
      theMostClient
        .model("students/me/registrations")
        .asQueryable()
        .expand(
          "classes($orderby=semester,course/displayCode;$expand=course($expand=locale)," +
            "courseClass($expand=instructors($expand=instructor($select=InstructorSummary))),courseType($expand=locale))"
        )
        .orderBy("registrationYear desc")
        .thenBy("registrationPeriod desc")
        .getItems()
        .then((res) => {
          if (!ignore) {
            setAllRegistrations(res);
          }
        });
    }

    return () => {
      ignore = true;
    };
  }, [theMostClient]);

  return { allRegistrations };
}
