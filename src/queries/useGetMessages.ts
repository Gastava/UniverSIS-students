import { useTheMostClientContext } from "@/context/ApplicationContext";
import { useEffect, useState } from "react";


export function useGetAllMessages(){
    const theMostClient = useTheMostClientContext();
    const [allMessages, setAllMessages] = useState<any[]>([]);

    useEffect(() =>{
        if (process.env.NEXT_PUBLIC_MOCKED === "true") {
            //! SET MOCK
          }else{
            theMostClient.model('students/me/messages')
            .asQueryable()
            .expand('recipient, attachments,action($expand=actionStatus), sender')
            .orderByDescending('dateCreated')
            .getList()
            .then((messages) => {
                setAllMessages(messages.value)
            })
        }
    }, [ theMostClient]);

    return allMessages;
}


export function useGetAllUnreadMessages(){
    const theMostClient = useTheMostClientContext();
    const [allUnreadMessages, setAllUnreadMessages] = useState<any[]>([]);

    useEffect(() =>{
        if (process.env.NEXT_PUBLIC_MOCKED === "true") {
            //   SET MOCK 
          }else{
            theMostClient.model('students/me/messages')
            .asQueryable()
            .expand('recipient, attachments, action($expand=actionStatus), sender')
            .orderByDescending('dateCreated')
            .where('dateReceived').equal(null)
            .getList()
            .then((unreadMessages) => {
                setAllUnreadMessages(unreadMessages.value)
            })
        }
    }, [ theMostClient]);

    return allUnreadMessages;
}

export function useGetAllReadMessages(){
    const theMostClient = useTheMostClientContext();
    const [allReadMessages, setAllReadMessages] = useState<any[]>([]);

    useEffect(() =>{
        if (process.env.NEXT_PUBLIC_MOCKED === "true") {
             //! SET MOCK
          }else{
            theMostClient.model('students/me/messages')
            .asQueryable()
            .expand('recipient, attachments, action($expand=actionStatus), sender')
            .orderByDescending('dateCreated')
            .where('dateReceived').notEqual(null)
            .getList()
            .then((readMessages) => {
                setAllReadMessages(readMessages.value)
            })
        }
    }, [ theMostClient]);

    return allReadMessages;
}


