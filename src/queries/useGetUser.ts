import { useTheMostClientContext } from "@/context/ApplicationContext";
import { useEffect, useState } from "react";
import { userMock } from "./mocks";

function useGetUser() {
  const theMostClient = useTheMostClientContext();

  const [user, setUser] = useState<any>();

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setUser(userMock);
    } else {
      theMostClient
        .model("students/me")
        .asQueryable()
        .expand("user", "department", "studyProgram", "inscriptionMode", "person") // problem with "studentSeries",
        .getItem()
        .then((student) => {
          theMostClient
            .model("LocalDepartments")
            .asQueryable()
            .where("id")
            .equal(student.department.id)
            .expand("organization($expand=instituteConfiguration,registrationPeriods)")
            .getItem()
            .then((department) => {
              student.department = department;
              setUser(student);
            });
        });
    }
  }, [theMostClient]);

  return user;
}

export default useGetUser;
