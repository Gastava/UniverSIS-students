import { useTheMostClientContext } from "@/context/ApplicationContext";
import { anyOf } from "@themost/query";
import { useEffect, useState } from "react";
import { currentRegistrationMocked } from "./mocks";

function useGetRecentCourses() {
  const theMostClient = useTheMostClientContext();

  const [currentRegistration, setCurrentRegistration] = useState<any>({});

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setCurrentRegistration(currentRegistrationMocked);
    } else {
      theMostClient
        .model("Students/Me/CurrentRegistration")
        .asQueryable()
        .expand(
          anyOf((x: any) => {
            x.classes;
          }).expand(
            anyOf((x: any) => {
              x.courseType;
            }).expand(
              anyOf((x: any) => {
                x.locale;
              })
            ),
            anyOf((x: any) => {
              x.courseClass;
            }).expand(
              anyOf((x: any) => {
                x.course;
              }).expand(
                anyOf((x: any) => {
                  x.locale;
                })
              ),
              anyOf((x: any) => {
                x.instructors;
              }).expand(
                anyOf((x: any) => {
                  x.instructor;
                }).select((x: any) => {
                  x.instructorSummary;
                })
              )
            )
          )
        )
        .getItem()
        .then((result) => {
          if (result != null) {
            setCurrentRegistration(result);
          }
        });
    }
  }, [theMostClient]);

  return { currentRegistration };
}

export default useGetRecentCourses;
