import { useTheMostClientContext } from "@/context/ApplicationContext";
import { currentRegistrationEffectiveStatusMock } from "@/queries/mocks";
import { useEffect, useState } from "react";

interface effectiveStatus {
  id: number;
  code: string;
  status: string;
  identifier: any;
  additionalType: any;
  alternateName: any;
  description: any;
  image: any;
  name: string;
  url: any;
  dateCreated: string;
  dateModified: string;
  createdBy: number;
  modifiedBy: number;
}

export default function useGetCurrentRegistrationEffectiveStatus() {
  const theMostClient = useTheMostClientContext();

  const [effectiveStatus, setEffectiveStatus] = useState<effectiveStatus | null>(null);

  useEffect(() => {
    let ignore = false;

    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setEffectiveStatus(currentRegistrationEffectiveStatusMock);
    } else {
      theMostClient
        .model("students/me/currentRegistration/effectiveStatus")
        .asQueryable()
        .getItem()
        .then((registrationEffectiveStatus) => {
          if (!ignore) {
            setEffectiveStatus(registrationEffectiveStatus);
          }
        });
    }

    return () => {
      ignore = true;
    };
  }, [theMostClient]);

  return { effectiveStatus };
}
