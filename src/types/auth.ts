// TODO: change to actual User type later
export interface User {
  id: number;
  access_token: string;
  accountType: number;
  name: string;
  alternateName: string;
  groups: { name: string; alternateName: string }[];
}
